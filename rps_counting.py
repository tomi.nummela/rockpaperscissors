

# Rock paper scissors vs cpu. Cpu is taught to swap hand to a hand that wins the previous hand when losing and
# to swap to human players previous hand when winning.
# Common tactic according chinese* research was that winning hand has a tendency to stay the same, while losing hand
# tends to switch.
# Intention is to put up a counter of turns this session,  human wins, cpu wins, and individual r,p,s wins.
# Maybe give a possibility to see ALL of the stats by quitting and giving the option?
# How to make this save the results forever is a mystery to me still. database? write and read from txt file?

import random

global round_count
global human_hand_last
global human_2nd_last
global cpu_hand_last
global cpu_2nd_last
global inp_list
inp_list = ["r","p","s","stat","q","in_fo"]
round_count, human_win_count, cpu_win_count = 1, 0, 0
human_input_counter = {"Rock": 0, "Paper": 0, "Scissors": 0}
cpu_input_counter = {"Rock": 0, "Paper": 0, "Scissors": 0}
wins_by_key = {"Rock": 0, "Paper": 0, "Scissors": 0}
#These will be modified during game and are prenamed only to not proc a part of cpu:s logic.
human_hand_last = "a"
human_2nd_last = "b"
cpu_hand_last = "c"
cpu_2nd_last = "d"

#Printout for Human And CPU last 2 hands
def in_fo():
    print("HL: {} H2L: {} CL: {} C2L: {}.".format(human_hand_last,human_2nd_last,cpu_hand_last,cpu_2nd_last))

#Printout for game stats, rounds, player/cpu wins, wins by hand and overall picks for both.
def statsprint():
    print(">Current Round: {}  ---  Player wins: {}  ---  CPU Wins: {}".format(round_count, human_win_count,
                                                                               cpu_win_count))
    print(">Player Stats ---> {}\n>CPU Stats ------> {}".format(human_input_counter, cpu_input_counter))
    print(">Wins by object -> {}".format(wins_by_key))
    print("----------------------------------------------------------------------")

#Combined with check_winner to build CPU logic and to check winner (surprise surprise)
who_wins = {("Rock", "Scissors"): True,
            ("Paper", "Rock"): True,
            ("Scissors", "Paper"): True}


def check_winner(player, cpu):
    result = who_wins.get((player, cpu), False)
    return result

#This is to pick a hand not picked at all last turn in the event that CPU wins
def hand_not_played(last_h_hand,last_cpu_hand):
    for e in choices:
        if e != human_hand_last and e != cpu_hand_last:
            return e

#This counters Human hand in the event that Human wins
def cpu_counter():
    if human_hand_last == "Rock":
        return "Paper"
    elif human_hand_last == "Paper":
        return "Scissors"
    elif human_hand_last == "Scissors":
        return "Rock"
    else:
        print("Something went wrong, counter logic")


def cpu_first_and_reset():
    global choices
    global cpu_inp
    #First round is always random, tie round also randoms.
    if round_count <= 1 or cpu_hand_last == human_hand_last:
        choices = ["Rock", "Paper", "Scissors"]
        cpu_inp = str(random.choice(choices))
        return cpu_inp
    elif check_winner(human_hand_last,cpu_hand_last) == False:
        cpu_inp = hand_not_played(human_hand_last,cpu_hand_last)
        return cpu_inp
    elif check_winner(cpu_2nd_last,human_2nd_last) == False and check_winner(cpu_hand_last,human_hand_last) == False:
        while True:
            cpu_inp = str(random.choice(choices))
            if cpu_inp == human_hand_last:
                continue
            else:
                return cpu_inp
    elif check_winner(human_hand_last,cpu_hand_last) == True:
        cpu_inp = cpu_counter()
        return cpu_inp
    else:
        print("Something went wrong, last line of CPU Logic")
        print(in_fo())


#I dont know if the game_on boolean is necessary or if it even works
while True:
    game_on = True
    cpu_first_and_reset()

    if round_count == 11:
        print("Ten games reached. Thank you for playing!")
        statsprint()
        print("End of loop")
        break
    else:
        print("Round {} / 10".format(round_count))
        human_inp = input("[R]ock, [P]aper, [S]cissors, [Q]uit or [Stat]istics")
        if human_inp.lower() == "r":
            human_inp = "Rock"
        elif human_inp.lower() == "p":
            human_inp = "Paper"
        elif human_inp.lower() == "s":
            human_inp = "Scissors"
        elif human_inp.lower() == "stat":
            statsprint()
            continue
        elif human_inp.lower() == "in_fo":
            in_fo()
            continue
        elif human_inp.lower() == "q":
            print("You ended early.")
            quit()
        elif [x for x in inp_list if human_inp.lower() != x]:
            print("You must've misclicked")
            continue
        if human_inp.lower() != "q":
            result = check_winner(human_inp, cpu_inp)
            print("You: {}  ---  CPU: {}".format(human_inp, cpu_inp))
            human_input_counter[human_inp] += 1
            cpu_input_counter[cpu_inp] += 1
            round_count += 1
            cpu_2nd_last = cpu_hand_last
            cpu_hand_last = cpu_inp
            human_2nd_last = human_hand_last
            human_hand_last = human_inp
            if human_inp == cpu_inp:
                print("Tie Game!\n----------------------------------------------------------------------")
            elif result:
                human_win_count += 1
                wins_by_key[human_inp] +=1
                print("You Won!\n----------------------------------------------------------------------")
            else:
                cpu_win_count += 1
                wins_by_key[cpu_inp] += 1
                print("You Lost You Massive Knob!\n----------------------------------------------------------------------")
        else:
            game_on = False
            quit()

if __name__ == '__main__':
    game_on = True